//import { useState } from "react";
import styled from "styled-components";
import { login } from "../redux/apiCalls";
import { mobile } from "../responsive";
//import { useDispatch, useSelector } from "react-redux";

import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
//import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(
      rgba(255, 255, 255, 0.5),
      rgba(255, 255, 255, 0.5)
    ),
    url("https://images.pexels.com/photos/6984661/pexels-photo-6984661.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940")
      center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 40%;
  padding: 20px;
  background-color: white;
  ${mobile({ width: "75%" })}
`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 20px 10px 0px 0px;
  padding: 10px;
`;

const Agreement = styled.span`
  font-size: 12px;
  margin: 20px 0px;
`;

const Button = styled.button`
  width: 40%;
  border: none;
  padding: 15px 20px;
  background-color: teal;
  color: white;
  cursor: pointer;
`;

const Register = () => {

const {user} = useContext(UserContext);
  const history = useNavigate();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [username, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

 // const dispatch = useDispatch();
//  const { isFetching, error } = useSelector((state) => state.user);

  // const handleClick = (e) => {
  //   e.preventDefault();
  //   login(dispatch, { firstName, lastName, userName, email, password1, password2 });
  // };


console.log(email);
    console.log(password1);
    console.log(password2); 

    function registerUser(e){
        e.preventDefault();

        fetch('https://frozen-everglades-86198.herokuapp.com/api/users/checkEmail', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

                if(data === true){

                    Swal.fire({
                        title: 'Duplicate email found',
                        icon: 'error',
                        text: 'Kindly provide another email to complete the registration.'  
                    });

                    

                }else {

                        fetch('https://frozen-everglades-86198.herokuapp.com/api/auth/register', {
                            method: "POST",
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                firstName: firstName,
                                lastName: lastName,
                                username: username,
                                email: email,
                                password: password1
                            })
                        })
                        .then(res => res.json())
                        .then(data => {

                            console.log(data);
                            //console.log(data.length);

                            if (data !== null || data !== undefined ) {

                                // Clear input fields
                                setFirstName('');
                                setLastName('');
                                setUserName('');
                                setEmail('');
                                setPassword1('');
                                setPassword2('');

                                Swal.fire({
                                    title: 'Registration successful',
                                    icon: 'success',
                                    text: 'Welcome to CAMILLE!'
                                });

                                history("/login");

                            } else {

                                Swal.fire({
                                    title: 'Something wrong',
                                    icon: 'error',
                                    text: 'Please try again.'   
                                });

                            }
                        })
                    };



        })


        // Clear input fields
        setFirstName('');
        setLastName('');
        setUserName('');
        setEmail('');
        setPassword1('');
        setPassword2('');

        //alert('Thank you for registering!');
    }



  useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && username !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [firstName, lastName, username, email, password1, password2]);

  return (

    (user.id !== null) ?
            <Navigate to="/login" />
    :
    <Container>
      <Wrapper onSubmit={(e) => registerUser(e)}>
        <Title>CREATE AN ACCOUNT</Title>
        <Form>
          <Input
            type="text"
            placeholder="First Name"
            onChange={(e) => setFirstName(e.target.value)}
            required
          />
          <Input
            type="text"
            placeholder="Last Name"
            onChange={(e) => setLastName(e.target.value)}
            required
          />
          <Input
            type="text"
            placeholder="Username"
            onChange={(e) => setUserName(e.target.value)}
            required
          />
          <Input
            type="email"
            placeholder="Email"
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Input
            type="password"
            placeholder="Password"
            onChange={(e) => setPassword1(e.target.value)}
            required
          />
          <Input
            type="password"
            placeholder="Confirm Password"
            onChange={(e) => setPassword2(e.target.value)}
            required
          />
          <Agreement>
            By creating an account, I consent to the processing of my personal
            data in accordance with the <b>PRIVACY POLICY</b>
          </Agreement>
          { isActive ?
                <Button type="submit" id="submitBtn">
                  CREATE
                </Button>
            :
                <Button type="submit" id="submitBtn" disabled>
                  CREATE
                </Button>
          }
        </Form>
      </Wrapper>
    </Container>
  );
};

export default Register;