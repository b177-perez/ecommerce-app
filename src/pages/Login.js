import { useState, useEffect, useContext } from 'react';
import styled from "styled-components";
//import { login } from "../redux/apiCalls";
import { mobile } from "../responsive";
//import { useDispatch, useSelector } from "react-redux";

import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(
      rgba(255, 255, 255, 0.5),
      rgba(255, 255, 255, 0.5)
    ),
    url("https://images.pexels.com/photos/6984650/pexels-photo-6984650.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940")
      center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 25%;
  padding: 20px;
  background-color: white;
  ${mobile({ width: "75%" })}
`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 10px 0;
  padding: 10px;
`;

const Button = styled.button`
  width: 40%;
  border: none;
  padding: 15px 20px;
  background-color: teal;
  color: white !important;
  cursor: pointer;
  margin-bottom: 10px;
  &:disabled {
    color: green;
    cursor: not-allowed;
  }
`;

const Link = styled.a`
  margin: 5px 0px;
  font-size: 12px;
  text-decoration: underline;
  cursor: pointer;
`;

const Error = styled.span`
  color: red;
`;

const Login = () => {

  // Allows us to consume the User context object and it's properties to use for user validation
  const { user, setUser } = useContext(UserContext);

  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");

  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);

//  const dispatch = useDispatch();
//  const { isFetching, error } = useSelector((state) => state.user);

  // const handleClick = (e) => {
  //   e.preventDefault();
  //   login(dispatch, { username, password });
  // };


function authenticate(e) {

            // Prevents page redirection via form submission
            e.preventDefault();
            //   login(dispatch, { username, password });

            // Process a fetch request to the corresponding backen API
            /* Syntax:
                fetch('url', {options})
                .then(res => res.json())
                .then(data => {})
            */
            fetch('https://frozen-everglades-86198.herokuapp.com/api/auth/login',{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: username,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if(typeof data.access !== "undefined"){
                    // The JWT will be used to retrieve user information
                    localStorage.setItem('token', data.access);
                    //localStorage.setItem('_id', data._id);
                    retrieveUserDetails(data.access);
                    // Sweet alert message
                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome to CAMILLE!"
                    })
                }
                else{
                    Swal.fire({
                        title: "Authentication failed",
                        icon: "error",
                        text: "Check your login details and try again."
                    })
                }
            })

            // Set the email of the user in the local storage
            /* Syntax:
                localStorage.setItem('propertyName', value);
            */
            // localStorage.setItem('email', email);

            // Set the global user state to have properties obtained from local storage
            // setUser({
            //     email: localStorage.getItem('email')
            // })

            // Clear input fields after submission
            setUserName('');
            setPassword('');

            // alert(`${email} has been logged in! Welcome back!`);

        }

        // "retrieveUserDetails" function to convert JWT from the fetch request
        const retrieveUserDetails = (token) => {  // remove ,_id
            fetch(`https://frozen-everglades-86198.herokuapp.com/api/auth/details`,{    //old = http://localhost:5000/api/users/find/${_id}
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        }

        useEffect(() => {

            // Validation to enable submit button when all fields are populated and both passwords match
            if(username !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [username, password]);

  return (

    (user.id !== null) ?
            <Navigate to="/" />
    :
    <Container>
      <Wrapper onSubmit={(e) => authenticate(e)}>
        <Title>SIGN IN</Title>
        <Form>
          <Input
            type="text" 
            placeholder="Username"
            onChange={(e) => setUserName(e.target.value)}
            required
          />
          <Input
            placeholder="Password"
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          />
          { isActive ? 
                <Button type="submit" id="submitBtn">
                    LOGIN
                </Button>
                : 
                <Button type="submit" id="submitBtn" disabled>
                    LOGIN
                </Button>
          }
          
          <Link>DO NOT YOU REMEMBER THE PASSWORD?</Link>
          <Link>CREATE A NEW ACCOUNT</Link>
        </Form>
      </Wrapper>
    </Container>
  );
};

export default Login;
