import styled from "styled-components";
import { mobile } from "../responsive";
import { Link, NavLink } from 'react-router-dom';

const Container = styled.div`
  height: 60vh;
  background-color: #fcf5f5;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const Title = styled.h1`
  font-size: 40px;
  margin-bottom: 20px;
`;

const Desc = styled.div`
  font-size: 24px;
  font-weight: 300;
  margin-bottom: 20px;
  ${mobile({ textAlign: "center" })}

`;

const Button = styled.button`
  width: 10%;
  border: none;
  padding: 15px 20px;
  background-color: teal;
  color: white;
  text-decoration: none;
  text-align: center;
`;



const ErrorContent = () => {

	// const data = {
 //        title: "404 - Not found",
 //        content: "The page you are looking for cannot be found",
 //        destination: "/",
 //        label: "Back home"
 //    }

  return (
  	<Container>
  		<Title>404 - Not found</Title>
      	<Desc>The page you are looking for cannot be found.</Desc>
      	<Button as={Link} to="/" >Return to Home</Button>
  	</Container>
  )

};

export default ErrorContent;
