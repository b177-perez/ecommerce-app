import Product from "./pages/Product";
import Home from "./pages/Home";
import ProductList from "./pages/ProductList";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import Cart from "./pages/Cart";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import Success from "./pages/Success";
import { useSelector } from "react-redux";

import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';

// <Route path="/register" element={user ? <Navigate to="/" /> : <Register />} />
// <Route path="/login" element={user ? <Navigate to="/" /> : <Login />} />


const App = () => {

//  const user = useSelector((state) => state.user.currentUser);
  
 // State hook for the user state that's defined for a global scope
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the use information is properly stored during login and localStorage information is cleared upon logout
  // useEffect(() => {
  //   console.log(user);
  //   console.log(localStorage);
  //   console.log(localStorage.token);
  // }, [user])



useEffect(() => {
        fetch('http://localhost:5000/api/auth/details', {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        })
        .then(res => res.json())
        .then(data => {

            if (typeof data._id !== 'undefined') {
                setUser({ id: data._id, isAdmin: data.isAdmin });
            } else {
                setUser({ id: null, isAdmin: null });
            }
        })

    }, [])



//removed unsetUser

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/products/:category" element={<ProductList />} />
          <Route path="/product/:id" element={<Product />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/success" element={<Success />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/register" element={<Register />} />
          <Route path="*" element={<Error />}/>
        </Routes> 
      </Router>
    </UserProvider>
  );
};

export default App;
